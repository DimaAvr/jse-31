package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.IAuthRepository;
import ru.tsc.avramenko.tm.api.service.IAuthService;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.api.service.IUserService;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyLoginException;
import ru.tsc.avramenko.tm.exception.empty.EmptyPasswordException;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @NotNull
    private final IAuthRepository authRepository;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(IAuthRepository authRepository, IUserService userService, IPropertyService propertyService) {
        this.authRepository = authRepository;
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public String getCurrentUserId() {
        @Nullable final String userId = authRepository.getCurrentUserId();
        if (userId == null) throw new EmptyIdException();
        return userId;
    }

    @Override
    public void setCurrentUserId(@Nullable final String userId) {
        authRepository.setCurrentUserId(userId);
    }

    @Override
    public boolean isAuth() {
        @Nullable final String currentUserId = authRepository.getCurrentUserId();
        return !(currentUserId == null || currentUserId.isEmpty());
    }

    @Override
    public boolean isAdmin() {
        @NotNull final String userId = getCurrentUserId();
        @NotNull final Role role = userService.findById(userId).getRole();
        return role.equals(Role.ADMIN);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || !hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        setCurrentUserId(user.getId());
    }

    @Override
    public void logout() {
        if (!isAuth()) throw new AccessDeniedException();
        System.out.println("See you soon, "
                + userService.findById(getCurrentUserId()).getFirstName()
                + "\n" + "- - - - -");
        setCurrentUserId(null);
    }

    @Override
    public void logoutData() {
        setCurrentUserId(null);
    }

    @Override
    public void checkRoles(@Nullable final Role... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final User user = userService.findById(getCurrentUserId());
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}