package ru.tsc.avramenko.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.model.AbstractEntity;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(final E entity);

    void remove(final E entity);

    @NotNull
    List<E> findAll();

    void clear();

    @NotNull
    E findById(@NotNull final String id);

    @NotNull
    E removeById(@NotNull final String id);

    void addAll(@NotNull List<E> entities);

}
