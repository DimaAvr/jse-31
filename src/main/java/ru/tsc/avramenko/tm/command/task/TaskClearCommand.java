package ru.tsc.avramenko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.enumerated.Role;

public class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Removing all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASK]");
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        serviceLocator.getTaskService().clear(userId);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}